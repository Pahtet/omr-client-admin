package com.omr.admin;

import java.net.UnknownHostException;
import java.util.Scanner;

import com.omr.data.Command;
import com.omr.data.CommandsEnum;

public class ConsoleComander implements Runnable
{
	private Scanner in;
	private boolean run;
	private Connect connect;
	ConsoleComander(Scanner in)
	{
		this.in=in;
	}
	
	@Override
	public void run()
	{
		run=true;
		while(run)
		{
			String command=in.nextLine();
			String[] commandArgs=command.split(" ");
			//System.out.println("commandArgs.lenght="+commandArgs.length);
			switch(commandArgs[0])
			{
			case "exit":
				run=false;
				System.out.println("Exiting program");
				break;
			case "connect":
				if(commandArgs.length!=3)
				{
					System.out.println("No args! Need connect ip port");
				}
				else
				{
					try {
						connect = new Connect(commandArgs[1],commandArgs[2]);
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					connect.connect();
					System.out.println("Connecting...");
				}
				break;
			case "disconnect":
				connect.disconnect();
				break;
			case "activate":
				sendCommand(new Command(CommandsEnum.ACTIVATE,Integer.parseInt(commandArgs[1])));
				break;
			case "deactivate":
				sendCommand(CommandsEnum.DEACTIVATE);
				break;
			case "password":
				sendCommand(CommandsEnum.PASSWORD);
				break;
			case "getmemoryusage":
				sendCommand(CommandsEnum.GETMEMORYUSAGE);
				break;
			case "runcommand":
				sendCommand(CommandsEnum.RUNCOMMAND);
				break;
			default:
				System.out.println("Incorrect command");
			}
		}	
	}
	
	private boolean sendCommand(CommandsEnum c)
	{
		return sendCommand(new Command(c,null));
	}
	
	private boolean sendCommand(Command c)
	{
		if(connect==null||!connect.isRun())
		{
			System.out.println("No connection");
			return false;
		}
		connect.send(c);
		return true;
	}
	
}
