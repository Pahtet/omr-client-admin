package com.omr.admin;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.omr.data.Command;
import com.omr.data.CommandsEnum;

public class Connect implements Runnable
{
	private int port;
	private InetAddress ipAddress;
	private boolean run=false;
	private ObjectOutputStream out;
	private ObjectInputStream in;
	private Socket socket;
	
	public boolean isRun()
	{
		return run;
	}
	
	public Connect(String ip,String port) throws UnknownHostException
	{
		ipAddress = InetAddress.getByName(ip);
		this.port=Integer.parseInt(port);
	}
	
	public void connect()
	{
		try
		{
			socket = new Socket(ipAddress,port);
			out=new ObjectOutputStream(socket.getOutputStream());
			in=new ObjectInputStream(socket.getInputStream());
			run=true;
			System.out.println("Connected");
			send(new Command(CommandsEnum.CONNECT,null));
			new Thread(this).start();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void disconnect()
	{
		run=false;
		try
		{
			in.close();
			out.close();
			socket.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run()
	{
		while(run)
		{
			try
			{
				Command c=(Command) in.readObject();
				processCommand(c);
			} catch (ClassNotFoundException | IOException e)
			{
				run=false;
				System.out.println("Agent disconnected");
			}
		}	
	}
	
	public void stop()
	{
		
	}
	
	public synchronized void send(Command c)
	{
		try {
			out.writeObject(c);
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			run=false;
		}
	}
	
	private boolean processCommand(Command c)
	{
		System.out.println("Command from agent "+c.getCommand().toString());
		switch(c.getCommand())
		{
		case MEMORYUSAGE:
			System.out.println("Memory usage "+(Long)c.getData()/1024.0+"Mb");
			break;
		}
		return true;
	}
}
